//@flow
import jsonData from '../assets/json/layoutPreloads.json'
import Cookies from 'universal-cookie';

const loadDefaultButtonLayouts = () => {
  return JSON.parse(JSON.stringify(jsonData));
}

const loadUserButtonLayouts = () => {
  const cookie = new Cookies("cardboard-bot-ui-UserSettings");
  return cookie.get("buttonLayouts");
}

const allButtonLayouts = () => {
  const defaultData = loadDefaultButtonLayouts();
  const userData = loadUserButtonLayouts();

  let data = [...defaultData.layouts];

  if(userData){
    data = [...data, ...userData];
  }

  return data
}

export default allButtonLayouts
