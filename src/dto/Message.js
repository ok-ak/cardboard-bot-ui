//@flow
export type ActionType = "holdOnOrOffFlipStateWhenPressedAgain" | "slowOscillateOnOffUntilPressedAgain" | "fastOscillateOnOffUntilPressedAgain";

export type Pins = {
  servo: Servo | typeof undefined,
  button: number[] | typeof undefined
}

export type Servo = {
  left: number[][] | typeof undefined,
  right: number[][] | typeof undefined,
  single: number[] | typeof undefined
}

export default class Message {
  action: ActionType;
  description: string;
  pins: Pins;
  text: string;
  x: number | typeof undefined;
  y: number | typeof undefined;
  scale_max: number | typeof undefined;
  keywords: string[] | typeof undefined;

  constructor(...args: any){
    const [action, description, pins, text, x, y, scale_max, keywords] = args;
    this.action = action? action : 'holdOnOrOffFlipStateWhenPressedAgain';
    this.description = description? description : '';
    this.pins = pins;
    this.text = text? text : '';
    this.x = x;
    this.y = y;
    this.scale_max = scale_max;
    this.keywords = !keywords? [] : undefined;
  }
}