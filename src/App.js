// @flow
import React from 'react';
import './App.css';
import GamePad from './components/GamePad/GamePad';
import allButtonLayouts from './services/LayoutService';

function App() {
  return (
    <div className="App">
      <GamePad buttonLayouts={allButtonLayouts()}/>
    </div>
  );
}

export default App;
