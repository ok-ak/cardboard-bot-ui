// @flow
import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField'

const WEBSOCKET_SERVER_URL = 'ws://127.0.0.1:8080'

class NameInverterWS extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      serverText: ''
    }
  }

  WebSocketClient = new WebSocket(WEBSOCKET_SERVER_URL);

  componentDidMount(){
    this.WebSocketClient.onopen = (event) => {
      console.log('WebSocket Bifrost has been opened');
    }
    this.WebSocketClient.onmessage = (event) => {
      // on receiving a message, add it to the list of messages
      console.log('!!!!!incoming message from the server',event.data)
      this.setState({
        serverText: event.data
      });
    }
  
    this.WebSocketClient.onclose = () => {
      console.log('WebSocket Bifrost has been closed')
      // automatically try to reconnect on connection loss
      // this.setState({
      //   ws: new WebSocket(URL),
      // })
    }
  }
  
  // WebSocketClient.onopen = function (event) {
  //   WebSocketClient.send('WebSocket Bifrost has been opened'); 

  //   // NOW WE CAN SEND AND RECEIVE DATA DUH
  // }
  
  render() {
 
    const wsSend = (event) => {
      this.WebSocketClient.send(event.target.value);
    }

    return (
      <div>
        <TextField
          id="standard-name"
          label="Name To Invert"
          onChange={wsSend}
          margin="normal"/>
        <br/>
        <TextField
          id="standard-name"
          label="Inverted Name"
          value={this.state.serverText}
          disabled
          margin="normal"/>
      </div>      
    )
  }
}

export default NameInverterWS