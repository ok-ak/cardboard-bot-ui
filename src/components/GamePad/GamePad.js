// @flow
import React, { Component } from 'react';
import PushButton from './PushButton/PushButton';
import Drawer from './Drawer/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import { Typography } from '@material-ui/core';
import JoyStickButton, { MAX_SCALE } from './JoyStickButton/JoyStickButton';
import Message from '../../dto/Message';

type ControlType = "Joystick" | "PushButton";

type ActionType = "holdUntilFurtherInput" | "singleFireAndRetract" | "turboFireAndRetract" | "holdAndRetractOnRelease";

type ButtonDetail = {
  id: number,
  text: string,
  controlType: ControlType,
  action: ActionType,
  pins: number[],
  description: string,
  backgroundColor: string,
  textColor: string,
  x: number,
  y: number
}

type ButtonLayout = {
  id: number,
  buttons: ButtonDetail[]
}

type Props = {
  buttonLayouts: ButtonLayout[]
}

type State = {
  errors: string[],
  buttonLayouts: ButtonLayout[],
  layoutId: number,
  webSocketOpen: boolean,
  webSocketConnectionAttempts: number,
  drawerOpen: boolean
}

const WEBSOCKET_SERVER_URL: string = "ws://127.0.0.1:8080";
const WebSocketClient: WebSocket = new WebSocket(WEBSOCKET_SERVER_URL);

// let WEBSOCKET_OPEN = false;

const styles = {
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  justifyContent: "space-between"
}

class GamePad extends Component<Props, State> {

  state: State = {
    drawerOpen: false,
    errors: [],
    buttonLayouts: this.props.buttonLayouts,
    layoutId: 0,
    webSocketOpen: false,
    webSocketConnectionAttempts: 0
  }

  componentDidMount(){
    WebSocketClient.onopen = (event: Event) => {
      console.log('WebSocket Bifrost has been opened');
      // WEBSOCKET_OPEN = true;
      this.setState({
        webSocketOpen: true,
        webSocketConnectionAttempts: 0
      })
    }
    WebSocketClient.onmessage = (event: Event) => {
      // on receiving a message, add it to the list of messages
      console.log('!!!!!incoming message from the server', event.data);
      if(event.data.errors && event.data.errors.length > 0){
        this.setState({
          errors: [...this.state.errors, ...event.data.errors]
        });
      }
    }
    WebSocketClient.onclose = () => {
      console.log('WebSocket Bifrost has been closed');
      // WEBSOCKET_OPEN = false;
      this.setState({
        webSocketOpen: false
      })
      // automatically try to reconnect on connection loss
      // this.setState({
      //   ws: new WebSocket(URL),
      // })
    }
  }

  joyStickHandler = (buttonDetail: ButtonDetail, ...args: any) => {
    let [{x, y}, ...misc] = args;

    if(x === undefined){
      x = null;
    } else if (x === null){
      x = 0;
    }

    if(y === undefined){
      y = null;
    } else if (y === null){
      y = 0;
    }
    console.log('joyStickHandler args:', args);
    let message: Message = new Message(
      buttonDetail.action,
      buttonDetail.description,
      {
        servo: {
          left: buttonDetail.pins,
          right: buttonDetail.pins
        }
      },
      buttonDetail.text,
      x,
      y,
      MAX_SCALE,
      misc
    )
    WebSocketClient.send(JSON.stringify(message));
  }

  clickButtonHandler = (buttonDetail: ButtonDetail, ...args: any) => {
    let [{x, y}, ...misc] = args;

    if(x === undefined){
      x = null;
    } else if (x === null){
      x = 0;
    }

    if(y === undefined){
      y = null;
    } else if (y === null){
      y = 0;
    }
    console.log('clickButtonHandler args:', args);
    let message: Message = new Message(
      buttonDetail.action,
      buttonDetail.description,
      {
        button: buttonDetail.pins
      },
      buttonDetail.text,
      x,
      y,
      null,
      misc
    )
    WebSocketClient.send(JSON.stringify(message));
  }

  addButtonHandler = (buttonDetail: ButtonDetail) => {
    const newButtonLayouts = this.state.buttonLayouts.map((buttonLayout) => {
      if(buttonLayout.id === this.state.layoutId){
        buttonLayout.buttons.push(buttonDetail)
      }
      return buttonLayout
    })
    this.setState({
      buttonLayouts: newButtonLayouts
    })
  }

  removeButtonHandler = (buttonDetail: ButtonDetail) => {
    const newButtonLayouts = this.state.buttonLayouts.map((buttonLayout) => {
      if(buttonLayout.id === this.state.layoutId){
        buttonLayout.buttons = buttonLayout.buttons.filter((prevButtonDetail) => {
          return buttonDetail.id !== prevButtonDetail.id
        });
      }
      return buttonLayout
    })
    this.setState({
      buttonLayouts: newButtonLayouts
    })
  }

  editButtonHandler = (buttonDetail: ButtonDetail) => {
    const newButtonLayouts = this.state.buttonLayouts.map((buttonLayout) => {
      if(buttonLayout.id === this.state.layoutId){
        buttonLayout.buttons = buttonLayout.buttons.map((prevButtonDetail) => {
          if(buttonDetail.id === prevButtonDetail.id) {
            return buttonDetail
          }
          return prevButtonDetail
        });
      }
      return buttonLayout
    })
    this.setState({
      buttonLayouts: newButtonLayouts
    })
  }

  addLayoutHandler = (buttonLayout: ButtonLayout) => {
    const prevButtonLayouts = this.state.buttonLayouts;
    let newButtonLayout = buttonLayout;
    let newId = prevButtonLayouts.slice(-1)[0].id? prevButtonLayouts.slice(-1)[0].id + 1 : 0;
    newButtonLayout.id = newId;
    
    const newButtonLayouts = this.state.buttonLayouts;
    newButtonLayouts.push(newButtonLayout);

    this.setState({
      buttonLayouts: newButtonLayouts
    });
  }

  editLayoutHandler = (buttonLayout: ButtonLayout) => {
    const newButtonLayouts = this.state.buttonLayouts.map((prevButtonLayout) => {
      if(prevButtonLayout.id === buttonLayout.id){
        return buttonLayout;
      }
      return prevButtonLayout;
    })
    this.setState({
      buttonLayouts: newButtonLayouts
    })
  }

  deleteLayoutHandler = (buttonLayout: ButtonLayout) => {
    const newButtonLayouts = this.state.buttonLayouts.filter((prevButtonLayout) => {
      return buttonLayout.id !== prevButtonLayout.id;
    })
  }

  toggleDrawerHandler = (open: boolean) => event => {
    if(event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')){
      return;
    }
    this.setState({drawerOpen: open})
  }

  toggleDrawerCloseHandler = () => {
    this.setState({drawerOpen: false})
  }

  toggleDrawerOpenHandler = () => {
    this.setState({drawerOpen: true})
  }

  getButtonsByLayoutId = (id: number): ButtonDetail[] => {
    const matchingButtonLayouts = this.state.buttonLayouts.filter(buttonLayout => buttonLayout.id === id );
    const buttonLayout = matchingButtonLayouts[0];
    return buttonLayout.buttons;
  }

  render() {
    const controlButtons: ButtonDetail[] = this.getButtonsByLayoutId(this.state.layoutId);

    return(
      <div>
        <AppBar position="static">
          <Toolbar variant="dense">
            <IconButton edge="start" color="inherit" onClick={this.toggleDrawerOpenHandler}>
              <MenuIcon />
            </IconButton>
            <div style={styles}>
              <Typography variant="h6">
                Cardboard Battle Bot
              </Typography>
              <Typography variant="h6">
                {this.state.webSocketOpen? "Bifrost Open": "Bifrost Closed"} 
              </Typography>
            </div>
          </Toolbar>
        </AppBar>
        <Drawer
          open = {this.state.drawerOpen}
          toggleDrawerOpenHandler = {this.toggleDrawerOpenHandler}
          toggleDrawerCloseHandler = {this.toggleDrawerCloseHandler}
          controlButtons = {() => {}}
          addButtonHandler = {this.addButtonHandler}
          editButtonHandler = {this.editButtonHandler}
          buttonLayouts = {this.state.buttonLayouts}
          addLayoutHandler = {this.addLayoutHandler}
          editLayoutHandler = {this.editLayoutHandler}
          deleteLayoutHandler = {this.deleteLayoutHandler} />

        {controlButtons.map((controlButton: ButtonDetail, index) => {
          if(controlButton.controlType === 'PushButton'){
            return (
              <PushButton
                key = {index}
                buttonDetail = {controlButton}
                clickButtonHandler = {this.clickButtonHandler} />
            )
          } else {
            return (
              <JoyStickButton
                key = {index}
                buttonDetail = {controlButton}
                joyStickHandler = {this.joyStickHandler} />
            )
          }
        })}
      </div>
    )
  }
}

export default GamePad;