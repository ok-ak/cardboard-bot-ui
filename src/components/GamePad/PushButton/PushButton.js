// @flow
import React from 'react';
import Fab from '@material-ui/core/Fab';
import { withStyles } from '@material-ui/core'

type ControlType = "Joystick" | "PushButton";

type ActionType = "holdUntilFurtherInput" | "singleFireAndReset" | "turboFireAndReset" | "holdAndRestOnRelease";

type ButtonDetail = {
  id: number,
  text: string,
  controlType: ControlType,
  action: ActionType,
  pins: number[],
  description: string,
  backgroundColor: string,
  textColor: string,
  x: number,
  y: number
}

type Props = {
  buttonDetail: ButtonDetail,
  clickHandler: any
}

const PushButton = (props: Props) => {

  const styles = {
    'zIndex': props.buttonDetail.id,
    'position': 'absolute',
    'left': props.buttonDetail.x.toString() + 'px',
    'top': props.buttonDetail.y.toString() + 'px'
  }

  const StyledButton = withStyles({
    root: {
      background: props.buttonDetail.backgroundColor || 'gray',
      color: props.buttonDetail.textColor || 'black',
      height: 48,
      width: 48,
      fontSize: 24,
      fontWeight: 'bolder'
    },
    label: {
      textTransform: 'capitalize',
    },
  })(Fab);

  return (
    <div style={styles}>
      <StyledButton
        className="PushButton"
        onTouchStart={() => props.clickButtonHandler(props.buttonDetail, 'onTouchStart', props.buttonDetail.action)}
        onTouchEnd={() => props.clickButtonHandler(props.buttonDetail, 'onTouchEnd', props.buttonDetail.action)}
        onClick={() => props.clickButtonHandler(props.buttonDetail, 'onClick', props.buttonDetail.action)} >
        {props.buttonDetail.text}
      </StyledButton>
    </div>
  )
}

export default PushButton;