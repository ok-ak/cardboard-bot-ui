//@flow
import React from 'react';
import ReactDOM from 'react-dom';
import { mount, shallow, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PushButton from './PushButton';
import { until, createShallow } from '@material-ui/core/test-utils';

type ButtonDetail = {
  id: number,
  text: string,
  controlType: ControlType,
  action: ActionType,
  pins: number[],
  description: string,
  backgroundColor: string,
  textColor: string,
  x: number,
  y: number
}

type ControlType = "Joystick" | "PushButton";

type ActionType = "holdUntilFurtherInput" | "singleFireAndReset" | "turboFireAndReset" | "holdAndRestOnRelease";

describe('PushButton', () => {
  it('renders with specified text', () => {
    //given
    let buttonDetail: ButtonDetail = {
      id: 1,
      text: 'some text',
      controlType: 'PushButton',
      action: 'singleFireAndReset',
      pins: [1, 2, 3],
      description: 'some description',
      backgroundColor: 'red',
      textColor: 'blue',
      x: 50,
      y: 100
    };

    let callback = () => {};

    //when
    const component = shallow(
      <PushButton
        buttonDetail={buttonDetail}
        clickHandler={callback} />
    ).find('.PushButton');

    //then
    expect(component.text()).toEqual(buttonDetail.text);
  });
});
