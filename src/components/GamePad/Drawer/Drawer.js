//@flow
import React, { Component } from 'react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import VideogameAssetIcon from '@material-ui/icons/VideogameAsset';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import ViewQuiltIcon from '@material-ui/icons/ViewQuilt';

type Props = {
  toggleDrawerCloseHandler: any,
  toggleDrawerOpenHandler: any,
  open: boolean
}

const buttons = [
  {
    text: 'Add Button',
    icon: <AddIcon />
  },
  {
    text: 'Edit Button',
    icon: <EditIcon />
  }
]

const buttonLayouts = [
  {
    text: 'Select Layout',
    icon: <VideogameAssetIcon />
  },
  {
    text: 'New Layout',
    icon: <NoteAddIcon />
  },
  {
    text: 'Edit Layout',
    icon: <ViewQuiltIcon />
  }
]

const Drawer = (props:Props) => {

    return(
      <SwipeableDrawer
        open={props.open}
        onClose={props.toggleDrawerCloseHandler}
        onOpen={props.toggleDrawerOpenHandler}>
          <div
            role="presentation"
            onClick={props.toggleDrawerCloseHandler}
            onKeyDown={props.toggleDrawerCloseHandler}>
            <List>
              {buttons.map((button, index) => (
                <ListItem button key={index}>
                  <ListItemIcon>{button.icon}</ListItemIcon>
                  <ListItemText primary={button.text} />
                </ListItem>
              ))}
            </List>
            <Divider />
            <List>
              {buttonLayouts.map((buttonLayout, index) => (
                <ListItem button key={index}>
                  <ListItemIcon>{buttonLayout.icon}</ListItemIcon>
                  <ListItemText primary={buttonLayout.text} />
                </ListItem>
              ))}
            </List>
          </div>
        </SwipeableDrawer>
    )
};

export default Drawer;