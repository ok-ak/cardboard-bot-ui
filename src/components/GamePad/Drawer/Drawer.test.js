//@flow
import React from 'react';
import ReactDOM from 'react-dom';
import { mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { until, createShallow } from '@material-ui/core/test-utils';
import Drawer from './Drawer';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

describe('Drawer', () => {
  it('has Add Button and AddIcon', () => {
    //given

    //when
    const shallow = createShallow()
    const component = shallow(
      <Drawer />
    );

    const addButtonText = component.find(ListItemText).at(0).props().primary;
    const addButtonIcon = component.find(ListItemIcon).at(0).props().children.type.displayName;

    //then
    expect(addButtonText).toEqual('Add Button');
    expect(addButtonIcon).toEqual('AddIcon');
  });
  it('has Edit Button and EditIcon', () => {
    //given

    //when
    const shallow = createShallow()
    const component = shallow(
      <Drawer />
    );

    const addButtonText = component.find(ListItemText).at(1).props().primary;
    const addButtonIcon = component.find(ListItemIcon).at(1).props().children.type.displayName;

    //then
    expect(addButtonText).toEqual('Edit Button');
    expect(addButtonIcon).toEqual('EditIcon');
  });
  it('has Select Layout and InputIcon', () => {
    //given

    //when
    const shallow = createShallow()
    const component = shallow(
      <Drawer />
    );

    const SelectLayoutText = component.find(ListItemText).at(2).props().primary;
    const SelectLayoutIcon = component.find(ListItemIcon).at(2).props().children.type.displayName;

    //then
    expect(SelectLayoutText).toEqual('Select Layout');
    expect(SelectLayoutIcon).toEqual('VideogameAssetIcon');
  });
  it('has Create New Layout and NoteAddIcon', () => {
    //given

    //when
    const shallow = createShallow()
    const component = shallow(
      <Drawer />
    );

    const newLayoutText = component.find(ListItemText).at(3).props().primary;
    const newLayoutIcon = component.find(ListItemIcon).at(3).props().children.type.displayName;

    //then
    expect(newLayoutText).toEqual('New Layout');
    expect(newLayoutIcon).toEqual('NoteAddIcon');
  });
  it('has Edit Current Layout and ViewQuiltIcon', () => {
    //given

    //when
    const shallow = createShallow()
    const component = shallow(
      <Drawer />
    );

    const EditLayoutText = component.find(ListItemText).at(4).props().primary;
    const EditLayoutIcon = component.find(ListItemIcon).at(4).props().children.type.displayName;

    //then
    expect(EditLayoutText).toEqual('Edit Layout');
    expect(EditLayoutIcon).toEqual('ViewQuiltIcon');
  });
});