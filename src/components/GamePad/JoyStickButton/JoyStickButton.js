import React, { Component } from 'react';
import { Joystick } from 'react-joystick-component';

export const MAX_SCALE: number = 50;

type ControlType = "Joystick" | "PushButton";

type ActionType = "holdUntilFurtherInput" | "singleFireAndReset" | "turboFireAndReset" | "holdAndRestOnRelease";

type ButtonDetail = {
  id: number,
  text: string,
  controlType: ControlType,
  action: ActionType,
  pins: number[],
  description: string,
  backgroundColor: string,
  textColor: string,
  x: number,
  y: number
}
 
const moveJoyStickHandler = (event) => {
    return {x: event.x, y: event.y};
}

const startJoyStickHandler = (event) => {
    console.log('joystick', event.type, ':', event.x, ',', event.y, ',', event.direction);
}

const stopJoyStickHandler = (event) => {
    console.log('joystick', event.type, ':', event.x, ',', event.y, ',', event.direction);
}

const JoyStickButton = (props: Props) => {

    const containerStyle = {
        'zIndex': props.buttonDetail.id,
        'position': 'absolute',
        'left': props.buttonDetail.x.toString() + 'px',
        'top': props.buttonDetail.y.toString() + 'px'
    }

    return (
        <div style={containerStyle}>
            <Joystick
                className = "JoyStickButton"
                size = {100}
                baseColor = {props.buttonDetail.backgroundColor}
                stickColor = {props.buttonDetail.textColor}
                disabled = {false}
                throttle = {10}
                // move = {(event) => moveJoyStickHandler(() => props.clickButtonHandler(), event, props.buttonDetail, 'move')}
                move = {(event) => props.joyStickHandler(props.buttonDetail, event, 'move')}
                stop = {(event) => props.joyStickHandler(props.buttonDetail, event, 'stop')}
                start = {startJoyStickHandler} />
            <div className = "JoyStickButton-title">{props.buttonDetail.text}</div>
        </div>
    )
}

export default JoyStickButton;